FROM postgres:9.6

LABEL name="PostgreSQL 9.6 Base Image (Spanish)" \
    maintainer="j.dasua@gmail.com"

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update \
    && apt-get --force-yes upgrade \
    && apt-get --force-yes dist-upgrade \
    && apt-get -y install apt-utils locales tzdata bash-completion less nano \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/apt/archive/*.deb \
    && localedef -i es_ES -c -f UTF-8 -A /usr/share/locale/locale.alias es_ES.UTF-8 \
    && mkdir -p /home/postgres \
    && chown postgres:postgres /home/postgres \
    && true
ENV LANG es_ES.UTF-8

RUN rm /etc/localtime
RUN echo "Atlantic/Canary" > /etc/timezone && ln -s /usr/share/zoneinfo/Atlantic/Canary /etc/localtime && dpkg-reconfigure -f noninteractive tzdata
