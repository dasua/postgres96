# PostgreSQL 9.6
Postgres 9.6 base system

To get more information, see the official doc from Doker Hub [Postgres](https://hub.docker.com/_/postgres/)

### Start postgres instance
```
$ docker run --name my_bbdd -p 5432:5432 -e POSTGRES_PASSWORD=mysecretpassword -e POSTGRES_INITDB_ARGS="--data-checksums" -d dasua/postgres:9.6
```
